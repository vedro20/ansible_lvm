### What is this repository for? ###

This repository contains Ansible playbooks to work with Linux LVM and filesystems:
collect information about FS, LV, VG, PV, extend sizes , etc.


### Playbooks ###

* fs_select.yml

This PB requires 2 variables:

target_host - host to run against

target_mount - mountpoint on the host

Variables can be entered from prompts or specified with --extra-vars, -e flags.

PB gathers stats about FS, LV, VG, PV and sets the following global variables:

target_dev - device mounted at target_mount

total_kb - fs size in Kb

free_kb - fs free space in Kb

tvol - target Logical Volume, where fs resides

tvg - target Volume Group

freegb, freeb - free space in Volume Group in Gb and Bytes.

target_pv - Physical Volumes, where Volume Group resides. 
This is hash of hashes, subset of ansible_lvm['pvs']

This PB is a foundation for subsequent PBs, which may use these global variables to manipulate LVM objects.


### Testing ###

$ ansible-playbook -i <inventory_file> fs_select.yml

$ ansible-playbook -i <inventory_file> fs_select.yml -e "target_host=eapd01 target_mount=/opt"


* fs_report_dh.yml

This PB runs filesystem report from ansible_mounts. It requires to to enter hostname from the prompt or use flag -e "target_host=<hostname>".


* vg_report_dh.yml

This PB runs VG report from ansible_lvm['vgs']. It requires to to enter hostname from the prompt or use flag -e "target_host=<hostname>".


* fs_extend.yml

This PB is based on fs_select.yml, described above. It requires 3 variables:

target_host - host to run against

target_mount - mountpoint on the host

xsize - size to extend fs by, size units are according to lvcreate(8).

Variables can be entered from prompts or specified with --extra-vars, -e flags.

Debug messages are printed during execution.

If all checks are passed successfully, PB runs command lvextend on remote host.

Similar results could be accomplished with lvol module.


### Testing ###

$ ansible-playbook -i <inventory_file> fs_extend.yml

$ ansible-playbook -i <inventory_file> fs_extend.yml -e "target_host=eapd01 target_mount=/opt xsize=5g"



* These playbooks have been tested from CLI on CentOS-7.x



### Who do I talk to? ###

* Repo owner: Greg Ivanov <greg.ivanov@ensono.com>
* Other contact:
